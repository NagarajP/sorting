package com.myzee.sorting;

//Java program for implementation of Bubble Sort
import java.util.Arrays;

public class BubbleSort {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a[]  = {2, 4, 6, 11, 5, 3, 17, 8};
		print(a);
		System.out.println();
		sort(a);
	}

	private static void print(int[] a) {
		// TODO Auto-generated method stub
		Arrays.stream(a).forEach(i -> System.out.print(i + ", "));
	}

	private static void sort(int[] a) {
		// TODO Auto-generated method stub
		int count = 1;
		for (int i = 0; i < a.length; i++) {
			for(int j = 0; j < a.length - 1; j++) {
				if(a[j] > a[j+1]) {
					int temp = a[j];
					a[j] = a[j+1];
					a[j+1] = temp;
					print(a);
					System.out.println("iteration - " + count++ + "\t[swap [ "+ a[j] +" and " + a[j+1] + "]]");
				}
				
			}
		}
	}

}
